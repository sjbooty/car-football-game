﻿using UnityEngine;
using System.Collections;

public class PlayerTwoScored : MonoBehaviour 
{
	GUIStyle scoreFont;
	
	
	public static int playerTwoScore = 0;
	IEnumerator OnCollisionEnter(Collision collisionInfo)
	{
		if (collisionInfo.collider.tag == "Football") {



			yield return new WaitForSeconds(2.0f);

			if (playerTwoScore == 2)									//if there has been a collision between the ball and the goal decision system, wait two seconds add one to the score and reset the positions, if the user has scored 3 goals load the that team wins level
			{
				Application.LoadLevel("Red Team Wins");
			}
			else
			{
				Application.LoadLevel (Application.loadedLevel);
				
				playerTwoScore++;
			}

		}
		
	}

	void Start()
	{
		
		scoreFont = new GUIStyle ();
		scoreFont.fontSize = 40;								//score gui styles
		scoreFont.normal.textColor = Color.red;
	}
	
	void OnGUI()
	{

		GUI.Label (new Rect (Screen.width * 0.0f, Screen.height * 0.94f, 200.0f, 30.0f), "Red: " + playerTwoScore.ToString ("#0"), scoreFont);				//displays the score
	}
	
}