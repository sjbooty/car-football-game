﻿using UnityEngine;
using System.Collections;

public class JumpPlayerOne : MonoBehaviour {
	
	
	public float jumpSpeed;
	public Rigidbody target;			//use the public item as a target for the jump

	
	void Start() {
		target = GetComponent<Rigidbody>();			
	}
	
	


	
	void Update()
	{ 
		
		
		if (Input.GetButtonDown ("360_YButton"))			//if the user presses the y button on the controller, jump forward and up slighlty
			
		{
			target.rigidbody.AddForce( jumpSpeed * target.transform.forward + new Vector3(0,4,0), ForceMode.VelocityChange);
		

		}

		if(Input.GetButtonDown ("360_BButton"))				//if the user presses the b button on the controller, jump right
		{
			target.rigidbody.AddForce( jumpSpeed * target.transform.right + new Vector3(0,0,0), ForceMode.VelocityChange);
		}



		if(Input.GetButtonDown ("360_XButton"))					//if the user presses the x button on the controller, jump left
		   {
			target.rigidbody.AddForce( jumpSpeed * target.transform.right * -1 + new Vector3(0,0,0), ForceMode.VelocityChange);
			}



		
	}
}
	
	



