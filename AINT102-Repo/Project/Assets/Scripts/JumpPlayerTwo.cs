﻿using UnityEngine;
using System.Collections;

public class JumpPlayerTwo : MonoBehaviour {
	
	
	public float jumpSpeed;
	public Rigidbody target;
	
	
	void Start() {
		target = GetComponent<Rigidbody>();			//use the public item as a target for the jump
	}
	
	
	
	
	
	void Update()
	{ 
		
		
		if (Input.GetButtonDown ("JumpForward"))			//if the user presses the numpad 8 button on the keyboard, jump forward and up slighlty
			
		{
			target.rigidbody.AddForce( jumpSpeed * target.transform.forward + new Vector3(0,4,0), ForceMode.VelocityChange);
			
			
		}
		
		if(Input.GetButtonDown ("JumpRight"))				//if the user presses the numpad 6 button on the keyboard, jump right
		{
			target.rigidbody.AddForce( jumpSpeed * target.transform.right + new Vector3(0,0,0), ForceMode.VelocityChange);
		}
		
		
		
		if(Input.GetButtonDown ("JumpLeft"))				//if the user presses the numpad 4 button on the keybaord, jump left
		{
			target.rigidbody.AddForce( jumpSpeed * target.transform.right * -1 + new Vector3(0,0,0), ForceMode.VelocityChange);
		}
		
		
		
		
	}
}


