﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {
	public GUISkin gSkin;
	public AudioClip target;
	//public AudioClip gameClip;
	void start() {
	

		}


	void OnGUI()
	{
		GUI.skin = gSkin;
		if (GUI.Button (new Rect ((Screen.width/2) - (Screen.width * 0.1f), Screen.height *0.2f, Screen.width * 0.2f, Screen.height * 0.1f), "Play With Keyboard")) 
			{
			Application.LoadLevel("Main Game Keyboard");				//load a level if the gui button is pressed.

			}

		if (GUI.Button (new Rect ((Screen.width/2) - (Screen.width * 0.1f), Screen.height *0.3f, Screen.width * 0.2f, Screen.height * 0.1f), "Play With Controller")) 
		{
			Application.LoadLevel("Main Game Controller");			//load a level if the gui button is pressed.
			
		}

		if (GUI.Button (new Rect ((Screen.width/2) - (Screen.width * 0.1f),Screen.height *0.4f, Screen.width * 0.2f, Screen.height * 0.1f), "Controls For Xbox Controller")) 
		{
			Application.LoadLevel("XboxControls");				//load a level if the gui button is pressed.
			
		}

		if (GUI.Button (new Rect ((Screen.width/2) - (Screen.width * 0.1f), Screen.height *0.5f, Screen.width * 0.2f, Screen.height * 0.1f), "Controls For Keyboard")) 
		{
			Application.LoadLevel("KeyboardControls");				//load a level if the gui button is pressed.
							
		}

		if (GUI.Button (new Rect ((Screen.width/2) - (Screen.width * 0.1f), Screen.height *0.6f, Screen.width * 0.2f, Screen.height * 0.1f), "Credits")) 
		{
			Application.LoadLevel("Credits");					//load a level if the gui button is pressed.
			
		}

		if (GUI.Button (new Rect ((Screen.width/2) - (Screen.width * 0.1f), Screen.height *0.7f, Screen.width * 0.2f, Screen.height * 0.1f), "Mute Sound")) 
		{
			if (AudioListener.volume == 1)
			AudioListener.volume = 0;						//if the audio listenr is on, turn it off and vice versa
			else 
			AudioListener.volume = 1;

			
		}
		if (GUI.Button (new Rect ((Screen.width/2) - (Screen.width * 0.3f), Screen.height *0.4f, Screen.width * 0.2f, Screen.height * 0.1f), "First Player To Score 3 Goals Wins")) 
		{

			
		}


	}
}
