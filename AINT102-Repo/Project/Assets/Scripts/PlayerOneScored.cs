﻿using UnityEngine;
using System.Collections;

public class PlayerOneScored : MonoBehaviour 
{
	GUIStyle scoreFont;


	public static int playerOneScore = 0;

	IEnumerator  OnCollisionEnter(Collision collisionInfo)
	{
				if (collisionInfo.collider.tag == "Football") {				//if there has been a collision between the ball and the goal decision system, wait two seconds add one to the score and reset the positions, if the user has scored 3 goals load the that team wins level
						



						yield return new WaitForSeconds(2.0f);

						if (playerOneScore == 2)
					{
						Application.LoadLevel("Blue Team Wins");
					}
					else
			{
						Application.LoadLevel (Application.loadedLevel);

						playerOneScore++;
			}
						
					


				}

		}

	void Start()
	{

				scoreFont = new GUIStyle ();			//score gui styles
				scoreFont.fontSize = 40;
		scoreFont.normal.textColor = Color.blue;
		

		}




		void OnGUI()
		{

		GUI.Label (new Rect (Screen.width * 0.0f, Screen.height * 0.0f, 200.0f, 30.0f), "Blue: " + playerOneScore.ToString ("#0"), scoreFont);				//displays the score

	}



}