﻿using UnityEngine;
using System.Collections;

public class TeamWins : MonoBehaviour {
	public GUISkin gSkin;


	void OnGUI()
	{
				GUI.skin = gSkin;

		
				if (GUI.Button (new Rect ((Screen.width / 2) - (Screen.width * 0.1f), Screen.height * 0.4f, Screen.width * 0.2f, Screen.height * 0.1f), "Main Menu")) {
				PlayerTwoScored.playerTwoScore = 0;
				PlayerOneScored.playerOneScore = 0;
				Application.LoadLevel ("MainMenu");					//display a button which takes you to the main menu and resets the scores when clicked.
			
				}
		}
}
