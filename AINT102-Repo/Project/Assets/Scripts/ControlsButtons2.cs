﻿using UnityEngine;
using System.Collections;

public class ControlsButtons2 : MonoBehaviour {

	public GUISkin gSkin;
	
	void OnGUI()		//This script is for the credits, its a simple gui button set which when clicked loads the other scenes.
	{
				GUI.skin = gSkin;
				if (GUI.Button (new Rect ((Screen.width * 0.8f), Screen.height / 2 + Screen.height * 0.0f, Screen.width * 0.2f, Screen.height * 0.1f), "Go Back")) {
						Application.LoadLevel ("MainMenu");
			
				}

		}
}
