﻿using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour {
	bool paused = false;
	public GUISkin gSkin;



	void Start() {
		Time.timeScale = 1f;
	}

	void Update()
	{
		if(Input.GetButtonDown("pauseButton"))
			paused = togglePause();
	}
	
	void OnGUI()
	{
		if(paused)
		{
			GUI.skin = gSkin;
			if (GUI.Button (new Rect ((Screen.width/2) - (Screen.width * 0.1f), Screen.height *0.2f, Screen.width * 0.2f, Screen.height * 0.1f), "Restart Round")) 
			{
				Application.LoadLevel(Application.loadedLevel);				//load a level if the gui button is pressed.
				
			}
			
			if (GUI.Button (new Rect ((Screen.width/2) - (Screen.width * 0.1f), Screen.height *0.3f, Screen.width * 0.2f, Screen.height * 0.1f), "Restart Game")) 
			{
				PlayerTwoScored.playerTwoScore = 0;							//load a level if the gui button is pressed.and reset the static score variables
				PlayerOneScored.playerOneScore = 0;

				Application.LoadLevel(Application.loadedLevel);
				
			}
			
			if (GUI.Button (new Rect ((Screen.width/2) - (Screen.width * 0.1f),Screen.height *0.4f, Screen.width * 0.2f, Screen.height * 0.1f), "Main Menu")) 
			{
				Application.LoadLevel("MainMenu");			//load a level if the gui button is pressed.
				
			}



			if (GUI.Button (new Rect ((Screen.width/2) - (Screen.width * 0.1f), Screen.height *0.5f, Screen.width * 0.2f, Screen.height * 0.1f), "Mute Sound")) 
			{
				if (AudioListener.volume == 1)
					AudioListener.volume = 0;							//if the audio listenr is on, turn it off and vice versa
				else 
					AudioListener.volume = 1;
				
				
			}
			

		}
	}
	
	bool togglePause()
	{
		if(Time.timeScale == 0f)		// if the pause button is pressed, change the time scale to 0, if it has already been pressed, change it back to 1
		{
			Time.timeScale = 1f;
			return(false);
		}
		else
		{
			Time.timeScale = 0f;
			return(true);    
		}
	}

}
